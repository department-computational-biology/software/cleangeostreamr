# Test Suite for CleanGeoStreamR Package

This folder contains the automated tests for the CleanGeoStreamR package, utilizing the `testthat` framework. Each test script corresponds to a specific set of functionalities in the package.

## Test Scripts

1. **test_CleanGeoStreamR_utils_functions**
   - Contains tests for the utility functions in the `CleanGeoStreamR` module.
   - Ensures that all utility functions perform as expected, including edge cases and error handling.

2. **test_read_curate_chemical_data_functions**
   - Tests the functions responsible for reading and curating chemical data.
   - Validates data parsing, cleaning processes, and data integrity checks.

3. **test_read_curate_spatial_data_functions**
   - Covers the tests for reading and curating spatial data.
   - Ensures correct data transformation, spatial operations, and accuracy of spatial data handling.

## Running the Tests

To execute all tests, use the following command in the R console:

```R
devtools::test()
