# CleanGeoStreamR Package

The CleanGeoStreamR package aims to provide automatic curation of spatially annotated data. Spatially annotated datasets often pose challenges due to inconsistencies, errors, and missing information. This package addresses these challenges by offering a series of functionalities to preprocess, curate, and finalize spatial data, ensuring its accuracy and integrity.

More details about the applied methods and development of CleanGeoStreamR can be found in the following scientific paper: [Automated curation of spatial metadata in environmental monitoring data (DOI: https://doi.org/10.1016/j.ecoinf.2025.103038)](https://doi.org/10.1016/j.ecoinf.2025.103038).

## Workflow Overview

![CleanGeoStreamR Workflow](CleanGeoStreamR_Workflow.png)

## Main Steps

The CleanGeoStreamR workflow can be summarized into four main steps:

### Initial Step

- **Source Functions:** Load necessary functions and libraries.
- **Parse Arguments:** Gather inputs from the user.
- **Read Required Files:** Import essential files needed for processing.

### Data Preprocessing

- **Prepare the empodat for further analysis:** Clean and format the data for subsequent operations.
- **Determine Unique Locations:** Identify unique locations within the dataset.
- **Normalize Locations:** Standardize location formats for consistency.

### Data Curation

- **Check Coordinate Format and Verify Country:** Ensure consistency in coordinate formats and validate associated country names.
- **Resolve Missing and Inconsistent Water Body and River Basin Names:** Address discrepancies in water body and river basin labels.
- **Assign Station Names via Geocoding and Reverse Geocoding:** Add or correct station names based on geographical coordinates.
- **Assign Average Coordinates to Stations with the Same Name:** Resolve duplicate station names by averaging their coordinates.
- **Integrate Manually Curated Data:** Incorporate manually curated data to enhance dataset accuracy.
- **Analyse Coordinate-Country Mismatch and Detect Cross-Border Cases:** Identify and rectify discrepancies between coordinates and associated countries, detecting cross-border data instances.

### Final Step

- **Create Additional Tags and Integrate Unique Locations Back to empodat File:** Add additional metadata tags and reintegrate curated locations back into the original dataset.
- **Generate Heatmap Regarding the Differing Entries:** Visualize discrepancies in the dataset using a heatmap.
- **Derive the Results of the Data Curation:** Summarize and present the results of the data curation process.

## Example Case Study
We provided the users with an example case study based on the chemical occurrence data downloaded from Norman on 24.04.2024.
The original chemical occurrence data includes more than 92 million lines and is more than 30 GB.
This file and all other related files can be downloaded from [Zenodo Dataset](https://zenodo.org/records/11395194). 
For simplicity, we included a significantly reduced but still representative version of the empodat file.
For more details, users can look at the 'example' directory.
This directory is also defined as the default directory of the project.
However,users can change this at any time

## Explanation of Modification Flags
We maintain a record of made modifications to each row using the "ModificationFLAGS" column. 
This column documents the alterations made to each specific row. 
These modifications fall under nine main cases, outlined as follows:

__Case 1:__ Involves resolving missing river basin and water body information in the dataset. 
This case resolves missing river basin and water body information in a dataset of stations by filtering out duplicates, 
extracting valid coordinates, and updating relevant columns with available data.

__Case 2:__ Addresses resolving missing river basin or water body information in a given dataset. 
This case also deals with duplicated station names. 
For these duplicates, the code verifies the presence of both river basin and water body names. 
If one is missing while the other is present, the missing information is replaced with the available one.

__Case 3:__ Focuses on resolving inconsistent and missing river basin and water body information in the dataset. 
It determines the most common river basin and water body names among duplicated stations and updates the values 
accordingly.

__Case 4:__ Includes processing station name information in the locations dataframe. 
The related script identifies and curates missing station names with valid coordinates using geocoding  
or reverse geocoding. 

__Case 5:__ Integrates manually curated information (from the previous studies) into the locations dataframe. 
The function curates water body names and river basin names based on previously prepared files, handles exceptions, 
and fills missing values accordingly.

__Case 6:__ Involves assigning average coordinates to stations with same names. 
The related script identifies stations with the same name, calculates the mean longitude and latitude for each group of 
stations, and assigns these average coordinates to the corresponding stations in the original dataframe.

__Case 7:__ Addresses curating controversial station country information. 
The function checks each station's country against world maps. 
If the country information is found to be controversial, it attempts to correct it based on the station's coordinates 
and nearby countries.

__Case 8:__ Focuses on cross-checking the country, latitude, and longitude information in the locations 
dataframe. 
The function utilizes world maps to verify if the coordinates fall within the corresponding country's boundaries.

__Case 9:__ Considers checking and formatting the locations dataframe. 
The function checks the format of latitude and longitude coordinates in the dataframe. 
If the format is incorrect, it attempts to correct it by modifying the dot position. 
Additionally, it checks if the coordinates fall within the corresponding country's boundaries using world maps.


## Usage

To utilize the functionalities of the CleanGeoStreamR package, follow these steps:

1. **Install the Package:**
```R
install.packages("CleanGeoStreamR")
```

2. **Load the Package:**
```R
library(CleanGeoStreamR)
```


## Contributing

Contributions to the CleanGeoStreamR package are welcome! If you encounter any issues, have suggestions for improvements, or would like to contribute new features, please refer to the contribution guidelines in the repository.

## Additional Information

For more detailed information on specific functions, arguments, and usage examples, please refer to the package documentation or visit the project repository.
