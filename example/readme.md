# Example Directory for CleanGeoStreamR Package

This directory is designed to help users understand the structure and workflow of the package by providing sample input files and designated directories for output results. This document will guide you through the organization and usage of the `example` directory.

## Directory Structure

The `example` directory is organized into three main subdirectories which are `input`,`output` and `rdata`.

### Input Directory

The `input` directory contains the necessary files required for the execution of the package functions. It is further divided into specific subfolders based on the type or category of input files. Each subfolder contains example files to illustrate the required format and structure.

**Subfolders:**
- `CompTox/`: Contains identifiers and CAS Registry Numbers (CASRN) from the CompTox Chemicals Dashboard. 
- `MANUAL_CURATION/`: Included files contain manually curated data for river basin names and water body names.
- `MISC/`: Included files are used for determining country codes and neighboring countries.
- `NaturalEarth/`: Files represent the oceans and are used to determine if given coordinates are located in marine areas.
- `NORMAN/`: These files are essential for the proper functioning of the R package and serve as examples of the required input data. It contains  three main files which are the chemical occurrence data of European fresh water bodies (empodat file), header file and the substance data. 


### Output Directory

The `output` directory is intended for storing the results of the code execution in `.csv` format. This helps in organizing the output files and making them easily accessible for further analysis or reporting. Default output directory is the one located in the `example` directory. However, user can change it anytime using the provided options. 


### Rdata Directory

The `rdata` directory is used for storing the results of the code execution in `.rdata` format. This is useful for saving R-specific data objects that can be loaded back into R for further manipulation or analysis.

## Running the Script

To execute the contents, run the `run_r_script.R` script included in this directory. This script includes calls to the main functions of the package, facilitating the execution and demonstration of functionality with the example data provided.