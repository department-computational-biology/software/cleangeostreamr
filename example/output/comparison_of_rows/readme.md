# Comparison of Rows Directory

The `comparison_of_rows` directory, located within the `output` directory, is designated for storing comparison results between consecutive location files. This directory is utilized primarily by the `read_curate_spatial_data.R` script, where comparisons are made between successive location files.

## Functionality

Within the `read_curate_spatial_data.R` script, there are cases where the `compare_location_files` function is called to compare consecutive location files. Users have the option to save the results of these comparisons directly to the `comparison_of_rows` directory as CSV files.

### Modifying the write_results Argument

To save comparison results to the `comparison_of_rows` directory, users can modify the `write_results` argument of the `compare_location_files` function. Set `write_results = TRUE` to enable saving the results as CSV files in the default `comparison_of_rows` directory.
