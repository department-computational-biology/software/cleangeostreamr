# Output Directory

This directory is used to store the output files generated with the execution of provided scripts. By default, the output will be stored in this directory, but the user has the option to change this setting if desired.

## Directory Structure

### Main Directory

- **Default Output Files**: All output files in `.csv` format will be stored in the main directory by default. These files contain the primary results of the program's execution.

### comparison_of_rows Directory

- **Comparison Files**: This subdirectory is designated for storing the results of comparing two consecutive location files. By default, this functionality is turned off. If the user changes the function argument to enable this feature, the comparison result files will be saved here.

## Customization

- **Changing Default Output Location**: Users can modify the default location for storing `.csv` files by specifying a different path in the program's configuration settings.
  
- **Saving Comparison Files**: To enable the storage of comparison files in the `comparison_of_rows` directory, set the relevant function argument to `true`. This will activate the comparison feature and save the results accordingly.
