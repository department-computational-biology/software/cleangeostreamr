# Example Input - CompTox Files

This README file provides detailed information regarding the contents of the files included in this directory. These files are essential for the proper functioning of the R package and serve as examples of the required input data.

## Included Files

### 1. DSSTox Identifiers and CASRN File: `DSSTox_Identifiers_and_CASRN_2021r1_REDUCED.csv`

This file contains identifiers and CAS Registry Numbers (CASRN) from the CompTox Chemicals Dashboard. It is a reduced version of the original dataset and includes essential chemical identifiers used within the package.

#### Description:
- **DSSTox Identifiers**: Unique identifiers for chemicals provided by the CompTox Chemicals Dashboard.
- **CASRN**: CAS Registry Numbers for the corresponding chemicals.
- **Preferred Name**: The preferred chemical name.
- **InChI**: The IUPAC International Chemical Identifier, a textual representation of the chemical substance.
- **InChIKey**: A hashed version of the full InChI, used for easier web searches of chemical information.

#### File Contents:
The file includes the following columns:
- `dtxsid`: DSSTox Substance Identifier
- `dtxcid`: DSSTox Compound Identifier
- `casrn`: CAS Registry Number
- `preferredName`: Preferred chemical name
- `inchi`: IUPAC International Chemical Identifier
- `inchiKey`: Hashed version of the InChI

#### Example:
Here is an example of the data structure:
```csv
"dtxsid","dtxcid","casrn","preferredName","inchi","inchiKey"
"DTXSID2020001","DTXCID2020001","50-00-0","Formaldehyde","InChI=1S/CH2O/c1-2/h1H2","WSFSSNUMVMOOMR-UHFFFAOYSA-N"
"DTXSID2020002","DTXCID2020002","75-07-0","Acetaldehyde","InChI=1S/C2H4O/c1-2-3/h2H,1H3","IKHGUXGNUITLKF-UHFFFAOYSA-N"