# Example Input - Manual Curation Files

This README file provides detailed information regarding the contents of the files included in this directory. These files contain manually curated data for river basin names and water body names.

## Included Files

### 1. River Basin Name Curated File: `river_basin_name_curated.csv`

This file contains curated river basin names used for standardizing and correcting river basin data within the package.

### 2. River Names Gaps Curated File: `river_names_gaps_curated.csv`

This file includes curated data for filling gaps in river names, ensuring consistency and completeness in the dataset.

### 3. Water Body Name Curated File: `water_body_name_curated.csv`

This file contains curated water body names, which are used to correct and standardize water body data within the package.

These curated files are essential for maintaining data integrity and consistency in river and water body names.
