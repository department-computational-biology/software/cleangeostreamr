# Preparing the "header.csv" File

This README file provides detailed instructions on how to prepare the "Surface_water_year-month-day_header.csv" file required for the proper use of our R package. This file is necessary for ensuring that the provided code functions correctly when working with new datasets related to surface waters. Below are the steps you need to follow:

## Steps to Prepare the Header CSV File

1. **Extract Initial Data:**
   - Obtain the first 100 lines of the `empodat` file (chemical occurrence data).
   - Save these lines as a new CSV file. You can use a command line tool or a script to achieve this. For example, using `head` in Linux:
     ```sh
     head -n 100 empodat.csv > Surface_water_year-month-day_header.csv
     ```

2. **Open the CSV File:**
   - Open the newly created `Surface_water_year-month-day_header.csv` file in a suitable program. For instance, you can use LibreOffice in Linux. Import the CSV file, ensuring the delimiter settings match your CSV format.

3. **Insert Empty Rows:**
   - Insert two empty rows right after the original header rows. Typically, in the case of the `empodat` file from NORMAN, the first three rows contain header information.

4. **Copy Example Rows:**
   - Navigate to the `example/input/NORMAN` directory within the project.
   - Open the provided example header file and copy the last two rows.

5. **Insert Copied Rows:**
   - Paste the copied rows into the two empty rows you added in your header file.

6. **Align Naming Structure:**
   - Ensure that the naming structure in the 4th row of your header file aligns with the provided example. The names should follow the same pattern.
   - If there is no specific reason, please do not change the provided naming structure there. Some of the names (for example `station_name`) have significant usage areas in the provided code.

7. **Handle New Columns:**
   - If the new dataset contains additional columns not present in the original header, assign dummy names to these new columns. For example, you can use names like `Other_1`, `Other_2`, etc.

8. **Check Column Types:**
   - For each column, enter `c` for character data types and `d` for numeric data types in the 5th row.
   - Ensure the data types match the content of each column.

9. **Finalize and Save:**
   - Remove all rows except the first five rows.
   - Save the file as `Surface_water_year-month-day_header.csv`.
