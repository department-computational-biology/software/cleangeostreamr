# Example Input - Natural Earth Files

This README file provides detailed information regarding the contents of the files included in this directory. These files represent the oceans and are used to determine if given coordinates are located in marine areas.

## Included Files

### 1. Ocean Shape File: `ne_10m_ocean.shp`

This file is a shapefile representing the oceans. It includes the following auxiliary files:
- `.shx`: Shape index file.
- `.dbf`: Attribute data in dBase format.
- `.prj`: Projection format.

#### Description:
- **Purpose**: Used to determine if given coordinates are located in marine areas.
- **Data Source**: Natural Earth, a public domain map dataset.

These files are essential for spatial analysis and geographical determinations within the package.
